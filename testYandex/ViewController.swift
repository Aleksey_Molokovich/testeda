//
//  ViewController.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//

import UIKit
import LGAlertView

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        viewModel.loadData()
        bindViewMOdel()
    }

    func bindViewMOdel()  {
        viewModel.updateView = {[unowned self] in
            self.tableView.reloadData()
        }
        
        viewModel.showAlert = { [unowned self] in
            LGAlertView(title: "Error",
                        message: "Что-то пошло не так",
                        style: .alert,
                        buttonTitles: ["Повторить"],
                        cancelButtonTitle: "Ок",
                        destructiveButtonTitle: nil,
                        actionHandler: { (alert, index, nil) in
                            self.viewModel.loadData()
            },
                        cancelHandler: nil,
                        destructiveHandler: nil).showAnimated()
        }
    }
    
    
    @IBAction func click(_ sender: Any) {
        let alert = LGAlertView(title: nil,
                    message: nil,
                    style: .actionSheet,
                    buttonTitles: ["SDWebImage","Kingfisher","Nuke","cleanCache"],
                    cancelButtonTitle: "Ок",
                    destructiveButtonTitle: nil,
                    actionHandler: { (alert, index, nil) in
                        self.viewModel.clearCache()
                        switch index {
                        case 0 :
                            self.viewModel.currentLoader = .kSDWebImage
                        case 1 :
                            self.viewModel.currentLoader = .kKingfisher
                        case 2 :
                            self.viewModel.currentLoader = .kNuke
                        case 3:
                            self.viewModel.clearCache()
                        default:
                            break
                        }
                        self.tableView.reloadData()
        },
                    cancelHandler: nil,
                    destructiveHandler: nil)
        
        alert.buttonsHeight = 44
        alert.showAnimated()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.configure(loader: viewModel.taskLoader(), place: viewModel.datasource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! TableViewCell).task?.cancel()
    }
    
    
    

}

