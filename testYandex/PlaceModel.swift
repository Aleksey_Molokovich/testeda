//
//  Model.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//

import ObjectMapper

class PlaceModel : Mappable{
    
    var id : Int?
    var name : String?
    var about : String?
    var picUrl : String?
    var picAspect : Double = 1.0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["place.id"]
        name <- map["place.name"]
        about <- map["place.footerDescription"]
        picUrl <- map["place.picture.uri"]
        picAspect <- map["place.picture.ratio"]
    }
    

}
