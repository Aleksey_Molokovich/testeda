//
//  ViewModel.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//
import Alamofire
import ObjectMapper

enum LoaderEnum {
    case kSDWebImage, kKingfisher, kNuke
}

class ViewModel {

    var updateView : (()->())?
    
    var showAlert : (()->())?
    
    var datasource : [PlaceModel] = []
    
    var currentLoader : LoaderEnum = .kSDWebImage
    
    func taskLoader() -> LoadImageProtocol {
        
        switch currentLoader {
        case .kSDWebImage:
            return SDWebImageLoader()
        case .kKingfisher:
            return KingfisherLoader()
        case .kNuke:
             return NukeLoader()
            
        }
        
    }
    
    func clearCache() {
        taskLoader().clearCache()
    }
    
    func loadData(){
        
        let url = URL(string: "https://eda.yandex/api/v2/catalog?latitude=55.762885&longitude=37.597360")!
        Alamofire.request(url).responseJSON {[unowned self]  response in
            
            let statusCode = response.response?.statusCode
            
            if let data = response.data, statusCode == 200
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: data)  as! [String : Any]
                    guard let payload =  json["payload"] as? [String : Any] else { return }
                    guard let foundPlaces =  payload["foundPlaces"] as? [[String : Any]] else { return }
                    
                    self.datasource  = Mapper<PlaceModel>().mapArray(JSONArray: foundPlaces )
                    
                    if self.updateView != nil {
                        self.updateView!()
                    }
                } catch {
                    
                }
            }else{
                if self.showAlert != nil {
                    self.showAlert!()
                }
            }
        }
        
    }
    
        
    }
    

