//
//  TableViewCell.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    var task : LoadImageProtocol?
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var aboutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(loader : LoadImageProtocol, place:PlaceModel){
        
        task = loader
        nameLabel.text = place.name
        aboutLabel.text = place.about
        guard var url = place.picUrl else {return}
        
        url = url.replacingOccurrences(of: "{w}", with: "100")
        url = "https://eda.yandex/" + url.replacingOccurrences(of: "{h}", with: "75")
        
        loader.load(url: url,
                    progress: { (progress) in
                        
        }, complition: {[unowned self] (image) in
            if Thread.isMainThread{
                 self.img.image = image
            }else{
                DispatchQueue.main.async {
                    self.img.image = image
                }
            }
            
            
        }) { (error) in
            
        }
        
        
        
        
    }

}
