//
//  NukeLoader.swift
//  testYandex
//
//  Created by Алексей Молокович on 02/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//

import Nuke

class NukeLoader: LoadImageProtocol {
    var task : ImageTask?
    
    func cancel() {
        if task == nil { return }
        task!.cancel()
    }
    
    func clearCache() {
        ImageCache.shared.removeAll()
    }
    
    func load(url: String, progress: @escaping HTTPOperationProgressBlock, complition: @escaping HTTPOperationCompletionBlock, error: @escaping HTTPOperationErrorBlock) {
        task = ImagePipeline.shared.loadImage(
            with: URL(string: url)!,
            progress: { _, receivedSize, expectedSize in
                if expectedSize != 0 {
                    progress(Double(receivedSize/expectedSize))
                }
        },
            completion: { response, err in
                if err == nil{
                    complition((response?.image)!)
                }else{
                    error(err! as NSError)
                }
        }
        )
        
        
    }
    

}
