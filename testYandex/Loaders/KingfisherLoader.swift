//
//  KingfisherLoader.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//

import Kingfisher

class KingfisherLoader: LoadImageProtocol {
    
    var task : RetrieveImageDownloadTask?
    
    func cancel() {
        if task == nil { return }
        task?.cancel()
    }
    
    func clearCache() {
        ImageCache.default.clearMemoryCache()
        ImageCache.default.clearDiskCache()
        ImageCache.default.cleanExpiredDiskCache()
    }
    
    func load(url: String, progress: @escaping HTTPOperationProgressBlock, complition: @escaping HTTPOperationCompletionBlock, error: @escaping HTTPOperationErrorBlock) {
        
        
        task = ImageDownloader.default.downloadImage(with: URL(string: url)!,
                                                     retrieveImageTask: nil,
                                                     options: nil,
                                                     progressBlock: { (receivedSize, expectedSize) in
                                                        if expectedSize != 0 {
                                                            progress(Double(receivedSize/expectedSize))
                                                        }
                                                        
        },
                                                     completionHandler: { (image, err, nil, data) in
                                                        if err == nil{
                                                            complition(image!)
                                                        }else{
                                                            error(err! as NSError)
                                                        }
        })
    }
    

}
