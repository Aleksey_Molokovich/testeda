//
//  LoadImageProtocol.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//


import UIKit

public typealias HTTPOperationCompletionBlock = (_ result: UIImage) -> Void
public typealias HTTPOperationProgressBlock = (_ result: Double) -> Void
public typealias HTTPOperationErrorBlock = (_ error: NSError) -> Void

protocol LoadImageProtocol{

    
    func cancel()
    func clearCache()
    func load(url : String,
              progress :@escaping HTTPOperationProgressBlock,
              complition :@escaping HTTPOperationCompletionBlock,
              error : @escaping HTTPOperationErrorBlock)
    
}
