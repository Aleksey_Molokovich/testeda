//
//  SDWebImageService.swift
//  testYandex
//
//  Created by Алексей Молокович on 01/10/2018.
//  Copyright © 2018 Алексей Молокович. All rights reserved.
//

import SDWebImage

class SDWebImageLoader : LoadImageProtocol {
    

    let manager = SDWebImageManager.shared()
    let downloader = SDWebImageDownloader.shared()
    var token : SDWebImageDownloadToken?
    
    func cancel() {
        downloader.cancel(token)
    }
    
    func clearCache() {
        manager.imageCache?.clearMemory()
        manager.imageCache?.clearDisk(onCompletion: nil)
    }
    
    func load(url: String,
              progress: @escaping HTTPOperationProgressBlock,
              complition: @escaping HTTPOperationCompletionBlock,
              error: @escaping HTTPOperationErrorBlock) {
        token = downloader.downloadImage(with: URL(string: url),
                                         options: .useNSURLCache,
                                         progress: { (receivedSize, expectedSize, nil) in
                                            if expectedSize != 0 {
                                                progress(Double(receivedSize/expectedSize))
                                            }
        },
                                         completed: { (image, nil, err, finished) in
                                            if err == nil{
                                                complition(image!)
                                            }else{
                                                error(err! as NSError)
                                            }
        })
        
        
    }
    

    
}
